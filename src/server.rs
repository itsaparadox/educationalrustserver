use std::io::{Result};
use std::net::{TcpListener, TcpStream};

pub fn launch(addr: String) -> Result<()> {
    let listener = TcpListener::bind(addr)?;

    /*
    for stream in listener.incoming() {
        handle(&mut stream?);
    }
    */

    Ok(())
}
