use std::io::{Result};
use super::*;

#[test]
pub fn test_server_launch() -> Result<()> {
    {
        let addr: String = "127.0.0.1:25560".to_string();
        server::launch(addr);
    }
    Ok(())
}
